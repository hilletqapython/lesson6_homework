"""
Task

Write the program "Cashier in the cinema", which will do the following:

Ask the user to enter their own age.

- if the user is less than 7 - display "You <>! Where are your parents?"

- if the user is less than 16 - display "You only <>, and this is an adult film!"

- if the user is more than 65 - display "You <>? Show your retirement ID!"

- if the user's age consists of the same numbers (11, 22, 44, etc., all possible options!) -
  display "Oh, you <>! What an interesting age!"

- in any other case - withdraw "Despite the fact that you <>, there are still no tickets!"

Instead of <> in each answer, substitute the meaning of age (number) and the correct form
of the word year

For any answer, the form of the word "year" must match the meaning of the user's age.


After receiving the answer, the program should ask the user if he wants to repeat
the execution. The user responds to Yes or No.
In the case when the user has answered Yes, the program repeats, No. - ends.
The user must be able to repeat the program an unlimited number of times.
"""


def age_input():
    """
    Asking user to enter his age
    Checking if entered age is correct(no text, no numbers below 0)
    Repeating a function again if there is an error in input
    :return: (int) user input
    """
    condition = True
    while condition:
        user_age = input("Enter your age in number(no text): ")
        if user_age.isdigit() and int(user_age) in range(1, 120):    # 1-year-old min/120 years old max
            condition = False
            return int(user_age)
        else:
            print("Wrong input. Enter your age correctly.")


def year_ending(age):
    """
    Checking what ending of a "рік" word we need
        If age ends with:
        1  - рік
        2, 3, 4 - роки
        0, 5, 6, 7, 8, 9 - років
    :param age: result of age_input function
    :return: (str) "рік", "роки", "років"
    """
    lst2 = [2, 3, 4]               # "роки"
    lst3 = [11, 12, 13, 14]        # "років"
    lst4 = [5, 6, 7, 8, 9, 0]      # "років"
    if age in lst3 or age % 10 in lst4:
        year = "років"
    elif age % 10 in lst2:
        year = "роки"
    else:
        year = "рік"
    return year


def age_check(age, year):
    """
    Check if user's age consists of the same numbers
    Check the age and return a right statement for that age
    :param age: user age (result of age_input function)
    :param year: one of "рік", "роки", "років" (result of year_ending function)
    :return (None) exit function in case age consists of the same numbers(11, 22, 33 etc.)
    """
    if age % 11 == 0 and age != 110:        # 110years % 11 = 0 (exception that needs to be removed)
        print(f"О, вам {age} {year}! Який цікавий вік!")
        return      # exit function if first and second digit same

    elif age < 7:
        print(f"Тобі ж {age} {year}! Де твої батьки?")
    elif age < 16:
        print(f"Тобі лише {age} {year}, а це е фільм для дорослих!")
    elif age > 65:
        print(f"Вам {age} {year}? Покажіть пенсійне посвідчення!")
    else:
        print(f"Незважаючи на те, що вам {age} {year}, білетів всеодно нема!")


def repeat_game():
    """
    Asking user if he wants to repeat the game
    :return: if "no" = exit loop and end game, if "yes" = repeating a game
    """
    while True:
        repeat = input("Do you want to repeat? Yes or No: ").lower()
        if repeat == "no":
            print("Program finished")
            return False
        else:
            return game()


def game():
    entered_age = age_input()
    year_end = year_ending(age=entered_age)
    age_check(age=entered_age, year=year_end)
    repeat_game()


game()


